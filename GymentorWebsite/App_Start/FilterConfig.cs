﻿using _Gymentor.App_Start;
using System.Web;
using System.Web.Mvc;

namespace GymentorWebsite
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new CompressionAttribute());
        }
    }
}
