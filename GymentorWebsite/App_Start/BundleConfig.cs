﻿using System.Web;
using System.Web.Optimization;

namespace GymentorWebsite
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/fontawesome").Include(
                    "~/Content/font-awesome.min.css"
                    ));

            bundles.Add(new StyleBundle("~/Content/unika_css/css").Include(
                    "~/Content/unika_css/bootstrap.min.css",
                    "~/Content/unika_css/animate.min.css",
                    "~/Content/font-awesome.min.css",
                    "~/Content/unika_css/owl.carousel.css",
                    "~/Content/unika_css/owl.theme.css",
                    "~/Content/unika_css/reset.css",
                    "~/Content/unika_css/style.css",
                    "~/Content/unika_css/mobile.css",
                    "~/Content/unika_css/cool-gray.css"
                    ));

            bundles.Add(new ScriptBundle("~/bundles/unika_js/jquery").Include(
                    "~/Scripts/unika_js/jquery-1.11.1.min.js",
                    "~/Scripts/unika_js/bootstrap.min.js",
                    "~/Scripts/unika_js/owl.carousel.min.js",
                    "~/Scripts/unika_js/jquery.stellar.min.js",
                    "~/Scripts/unika_js/wow.min.js",
                    "~/Scripts/unika_js/waypoints.min.js",
                    "~/Scripts/unika_js/isotope.pkgd.min.js",
                    "~/Scripts/unika_js/classie.js",
                    "~/Scripts/unika_js/jquery.easing.min.js",
                    "~/Scripts/unika_js/jquery.counterup.min.js",
                    "~/Scripts/unika_js/smoothscroll.js",
                    "~/Scripts/unika_js/theme.js"));

            bundles.IgnoreList.Clear();

        }
    }
}
